from frappe import _

def get_data():
	return [
		{
			"module_name": "Excel Meeting Booking",
			"type": "module",
			"label": _("Excel Meeting Booking")
		}
	]
